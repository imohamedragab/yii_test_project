<?php

use yii\db\Migration;

/**
 * Class m231113_184056_image
 */
class m231113_184056_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('image');
        echo "m231113_184056_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231113_184056_images cannot be reverted.\n";

        return false;
    }
    */
}
