<?php

use yii\db\Migration;

/**
 * Class m231113_185430_product
 */
class m231113_185430_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'image_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
        // Add foreign key for category_id
        $this->addForeignKey(
            'fk-product-category_id',
            'product',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product-image_id',
            'product',
            'image_id',
            'image',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product-category_id', 'product');
        $this->dropForeignKey('fk-product-image_id', 'product');
        $this->dropTable('product');
        echo "m231113_185430_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231113_185430_product cannot be reverted.\n";

        return false;
    }
    */
}
