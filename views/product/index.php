<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $products app\models\Product[] */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $products]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'category.title', // Assuming you have a relation in Product model to get the category
                'label' => 'Category',
            ],
            'description:ntext',
            [
                'attribute' => 'created_at',
                'format' => ['datetime', 'php:Y-m-d H:i:s'],
            ],
            [
                'label' => 'Image',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->image ? Html::img(Url::to($model->image->path, true), ['alt' => 'Image', 'style' => 'width:50px;']) : 'No Image';
                },
            ],
        ],
    ]); ?>

</div>
