
<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $categories app\models\Category[] */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

<h1><?= Html::encode($this->title) ?></h1>
<?= GridView::widget([
    'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $categories]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        'description:ntext',
    ],
]); ?>

</div>

