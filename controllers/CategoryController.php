<?php

namespace app\controllers;
use Yii;
use app\models\Category;


class CategoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $categories = Category::find()->all();

        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionCreate()
{
    $model = new Category();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        $category = new Category();
        $category->title = $model->title;
        $category->description = $model->description;

        if ($category->save()) {
            Yii::$app->session->setFlash('success', 'Category has been created successfully.');
            return $this->redirect(['create']);
        } else {
            Yii::$app->session->setFlash('error', 'Failed to save the category.');

        }
    }

    return $this->render('create', [
        'model' => $model,
    ]);
}

public function actionUpdate($id)
{
    $category = Category::findOne($id);

    if (!$category) {
        throw new \yii\web\NotFoundHttpException('Category not found.');
    }

    $model = new Category();
    $model->title = $category->title;
    $model->description = $category->description;

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        $category->title = $model->title;
        $category->description = $model->description;


        if ($category->save()) {
            Yii::$app->session->setFlash('success', 'Category has been updated successfully.');
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', 'Failed to update the category.');
        }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
}


}
