<?php

namespace app\controllers;
use Yii;
use app\models\Product;
use app\models\Image;
use yii\web\UploadedFile;

class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $products = Product::find()->all();

        return $this->render('index', [
            'products' => $products,
        ]);
    }

    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $product = new Product();
            $product->title = $model->title;
            $product->category_id = $model->category_id;
            $product->description = $model->description;

            // Handle image upload (you need to implement this)
            // Example: $product->image_id = $imageModel->id;
            if (Yii::$app->request->isPost) {
                $model->image = UploadedFile::getInstance($model, 'image');
            }

            // dd('sss',$model->title,$model->image);

            $image = $this->uploadImage($model->image);
            if ($image) {
                $product->image_id = $image->id;
            }
            if ($product->save()) {
                Yii::$app->session->setFlash('success', 'Product has been created successfully.');
                return $this->redirect(['index']);

            } else {
                Yii::$app->session->setFlash('error', 'Failed to save the product.');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $product = Product::findOne($id);

        if (!$product) {
            throw new \yii\web\NotFoundHttpException('Product not found.');
        }

        $model = new Product();
        $model->title = $product->title;
        $model->category_id = $product->category_id;
        $model->description = $product->description;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $product->title = $model->title;
            $product->category_id = $model->category_id;
            $product->description = $model->description;
            $image = $this->uploadImage($model->image);
            if ($image) {
                $product->image_id = $image->id;
            }

            if ($product->save()) {
                Yii::$app->session->setFlash('success', 'Product has been updated successfully.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Failed to update the product.');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    private function uploadImage($file)
        {
            if ($file) {
                $image = new Image();
                $image->title = $file->baseName;
                $image->name = $file->name;
                $image->path = 'uploads/' . $file->baseName . '.' . $file->extension;

                if ($file->saveAs($image->path) && $image->save()) {
                    return $image;
                }
            }

            return null;
        }


        public function getImage()
    {
        return $this->hasOne(Image::class, ['id' => 'image_id']);
    }

    // ... other methods ...

    // Setter for image_id attribute
    public function setImageId($value)
    {
        $this->image_id = $value;
    }

    // Getter for image_id attribute
    public function getImageId()
    {
        return $this->image_id;
    }

}
